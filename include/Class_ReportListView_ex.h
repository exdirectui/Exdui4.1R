#pragma once
#include "help_ex.h"

#define _rlv_sizeof 9*sizeof(size_t)
#define _rlv_nItemWidth 0
#define _rlv_nItemHeight 1
#define _rlv_cTCs 2
#define _rlv_pTCInfo 3
#define _rlv_arrTRInfo 4
#define _rlv_nTCIdxSorted 5
#define _rlv_hObjHead 6
#define _rlv_nHeadHeight 7
#define _rlv_fTCSortedDesc 8

#define _rlv_head_sizeof 8
#define _rlv_head_hListView 0
#define _rlv_head_nIndexHit 1

//获取命中列索引
#define RLVM_GETHITCOL 5901
//当删除表项
#define RLVN_DELETE_ITEM 97010
//当表头被单击
#define RLVN_COLUMNCLICK 97000

#define LVSICF_NOSCROLL 2

struct reportlistview_tc_s
{
	LPCWSTR wzText_;
	int nWidth_;
	int dwStyle_;
	int dwTextFormat_;
	int crText_;
	int dwState_;
};

struct reportlistview_tr_s
{
	void* pTDInfo_;
	int dwStyle_;
	size_t lParam_;
	EXHANDLE hImage_;
};

struct reportlistview_td_s
{
	LPCWSTR wzText_;
};

struct reportlistview_li_s
{
	int iRow_;
	int iCol_;
	int dwStyle_;
	LPCWSTR wzText_;
	EXHANDLE hImage_;
	size_t lParam_;
	int dwState_;
};

struct reportlistview_sort_s
{
	int iCol_;
	int ntype_;
	void* lpfnCmp_;
	bool fDesc_;
	size_t lParam_;
};

size_t CALLBACK _rlv_proc(HWND hWnd, EXHANDLE hObj, UINT uMsg, size_t wParam, size_t lParam);
size_t CALLBACK _rlv_head_proc(HWND hWnd, EXHANDLE hObj, UINT uMsg, size_t wParam, size_t lParam);
size_t _rlv_head_hittest(EXHANDLE hObj, int x, int y, bool fJustHit, int* rHitBlock);
void _rlv_head_paint(EXHANDLE hObj);
int _rlv_getHitCol(EXHANDLE hObj, int x);
bool _rlv_notify_proc(EXHANDLE hObj, EX_NMHDR* pNotifyInfo);
void _rlv_init(EXHANDLE hObj);
void _rlv_arr_del(array_s* hArr, int nIndex, reportlistview_tr_s* pvData, int nType);
size_t _rlv_arr_order(array_s* hArr, int nIndex1, void* pvData1, int nIndex2, void* pvData2, reportlistview_sort_s* pSortInfo, int nReason);
void _rlv_uninit(EXHANDLE hObj);
void _rlv_draw_tr(EXHANDLE hObj, EX_CUSTOMDRAW* pDrawInfo);
void _rlv_draw_td(EXHANDLE hObj, EX_CUSTOMDRAW* cd, int nIndexTR, int nIndexTC, reportlistview_tc_s* pTC, RECT* rcTD);
int _rlv_tc_ins(EXHANDLE hObj, reportlistview_tc_s* pInsertInfo);
bool _rlv_tc_del(EXHANDLE hObj, int nIndex);
void _rlv_tc_clear(EXHANDLE hObj);
void _rlv_tc_update(EXHANDLE hObj);
int _rlv_tr_ins(EXHANDLE hObj, reportlistview_tr_s* pInsertInfo);
bool _rlv_tr_del(EXHANDLE hObj, int nIndex);
void _rlv_tr_clear(EXHANDLE hObj);
void _rlv_tr_update(EXHANDLE hObj);
reportlistview_td_s* _rlv_td_get(EXHANDLE hObj, int nIndexTR, int nIndexTC);
void _rlv_td_setText(EXHANDLE hObj, int nIndexTR, int nIndexTC, LPCWSTR wzText);
bool _rlv_li_get(EXHANDLE hObj, reportlistview_li_s* lParam, bool fJustText);
int _lv_getitemstate(EXHANDLE hObj, int iItem);
bool _rlv_li_set(EXHANDLE hObj, reportlistview_li_s* lParam, bool fJustText);
void _rlv_regsiter();
