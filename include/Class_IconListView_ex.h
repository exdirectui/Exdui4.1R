#pragma once
#include "help_ex.h"

#define sizeof_ilv_item 3*sizeof(size_t)

//图标列表属性：表项宽度
#define _ilv_nWidth 0
//图标列表属性：表项高度
#define _ilv_nHeight 1
//图标列表属性：表项数组
#define _ilv_pArray 2
//图标列表属性：图片组句柄
#define _ilv_hImageList 3




size_t CALLBACK _IconListView_Proc(HWND hWnd, EXHANDLE hObj, UINT uMsg, size_t wParam, size_t lParam);
void _IconListView_Register();
EX_ICONLISTVIEW_ITEMINFO* _IconListView_OnArrAppend(array_s* pArray, int nIndex, EX_ICONLISTVIEW_ITEMINFO* pvItem, int nType);
void _IconListView_OnArrDelete(array_s* pArray, int nIndex, EX_ICONLISTVIEW_ITEMINFO* pvItem, int nType);
void _IconListView_Init(EXHANDLE hObj);
void _IconListView_Uninit(EXHANDLE hObj);
bool _IconListView_OnNotify(EXHANDLE hObj, size_t wParam, size_t lParam);
bool _IconListView_OnDrawItem(EXHANDLE hObj, EX_CUSTOMDRAW* cdr);