#pragma once
#include "help_ex.h"


typedef size_t(CALLBACK* LayoutPROC)(layout_s*, int, size_t, size_t);


struct layout_s
{
	void* lpLayoutInfo_;//父组件的布局信息
	void* lpfnProc_;//布局回调函数
	array_s* hArrChildrenInfo_; //处理的子组件属性数组
	int nType_; //布局类型
	EXHANDLE hBind_;//绑定的父组件(子组件是相对这个组件的)/HOBJ/HEXDUI
	int nBindType_;//绑定组件的类型,#HT_OBJECT或#HL_DUI
	int cbInfoLen_; //子组件属性长度(根据不同布局类型不同)
	EXHANDLE hLayout_; //本布局句柄
	bool fUpdateable_; //是否允许布局更新
};


bool _layout_register(int nType, LayoutPROC lpfnLayoutProc);
bool _layout_unregister(int nType);
void _layout_free_info(array_s* hArr, int nIndex, void* pvItem, int nType);
EXHANDLE _layout_create(int nType, EXHANDLE hObjBind);
EXHANDLE _layout_get_parent_layout(EXHANDLE hObj);
bool _layout_destroy(EXHANDLE hLayout);
bool _layout_enum_find_obj(void* hArr, int nIndex, void* pvItem, int nType, size_t pvParam);
void* _layout_get_child(layout_s* pLayout, EXHANDLE hObj);
bool _layout_update(EXHANDLE hLayout);
int _layout_gettype(EXHANDLE hLayout);
bool _layout_enableupdate(EXHANDLE hLayout, bool fUpdateable);
size_t _layout_notify(EXHANDLE hLayout, int nEvent, void* wParam, void* lParam);
bool _layout_table_setinfo(EXHANDLE hLayout, int* aRowHeight, int cRows, int* aCellWidth, int cCells);
bool _layout_setchildprop(EXHANDLE hLayout, EXHANDLE hObj, int dwPropID, int pvValue);
bool _layout_getchildprop(EXHANDLE hLayout, EXHANDLE hObj, int dwPropID, int* pvValue);
bool _layout_setprop(EXHANDLE hLayout, int dwPropID, int pvValue);
size_t _layout_getprop(EXHANDLE hLayout, int dwPropID);
bool _layout_absolute_setedge(EXHANDLE hLayout, EXHANDLE hObjChild, int dwEdge, int dwType, size_t nValue);
void _layout_move_margin(EXHANDLE hObj, RECT* lpObjRc, void* lpMargin, int dwLockFlags, int dwOrgFlags);
size_t CALLBACK __layout_linear_proc(layout_s* pLayput, int nEvent, size_t wParam, size_t lParam);
size_t CALLBACK __layout_flow_proc(layout_s* pLayout, int nEvent, size_t wParam, size_t lParam);
size_t CALLBACK __layout_page_proc(layout_s* pLayout, int nEvent, size_t wParam, size_t lParam);
size_t CALLBACK __layout_table_proc(layout_s* pLayout, int nEvent, size_t wParam, size_t lParam);
void _layout_relative_update(layout_s* pLayout, void* pLayoutInfo, array_s* hArrObjs, size_t lParam);
size_t CALLBACK __layout_relative_proc(layout_s* pLayout, int nEvent, size_t wParam, size_t lParam);
size_t CALLBACK __layout_absolute_proc(layout_s* pLayout, int nEvent, size_t wParam, size_t lParam);
bool _layout_addchild(EXHANDLE hLayout, EXHANDLE hObj);
bool _layout_addchildren(EXHANDLE hLayout, bool fDesc, int dwObjClassATOM, int* nCount);
bool _layout_deletechildren(EXHANDLE hLayout, int dwObjClassATOM);
bool _layout_deletechild(EXHANDLE hLayout, EXHANDLE hObj);
bool _layout_getchildproplist(EXHANDLE hLayout, EXHANDLE hObj, void** lpProps);
void* _layout_getproplist(EXHANDLE hLayout);
bool _layout_absolute_lock(EXHANDLE hLayout, EXHANDLE hObjChild, int tLeft, int tTop, int tRight, int tBottom, int tWidth, int tHeight);
void _layout_init();

