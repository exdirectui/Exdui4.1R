#include "help_ex.h"
#include "ImageList_ex.h"


BOOL _imglist_del(void* hImageList, size_t nIndex) {
	return Array_DelMember((array_s*)hImageList, nIndex);
}

BOOL _imglist_set(void* hImageList, int nIndex, void* pImg, int dwBytes)
{
    BOOL ret = FALSE;
    EXHANDLE hImg = _img_createfrommemory(pImg, dwBytes, NULL);
    if (hImg)
    {
        Array_SetMember((array_s*)hImageList, nIndex, hImg);
        ret = _img_destroy(hImg);
    }
    return ret;
}

BOOL _imglist_destroy(void* hImageList)
{
    return Array_Destroy((array_s*)hImageList);
}

BOOL _imglist_draw(void* hImageList, int nIndex, int hCanvas, int nLeft, int nTop, int nRight, int nBottom, int nAlpha)
{
    BOOL ret = 0;
    EXHANDLE hImg;
    int nSize;

    hImg = Array_GetMember((array_s*)hImageList, nIndex);
    nSize = Array_GetExtra((array_s*)hImageList);
    if (hImg)
    {
        int nWidth = nRight - nLeft;
        int nHeight = nBottom - nTop;
        ret = _canvas_drawimage(hCanvas, hImg, nLeft + (nWidth - LOWORD(nSize)) / 2, nTop + (nHeight - HIWORD(nSize)) / 2, nAlpha);
    }
    return ret;
}

int _imglist_count(void* hImageList)
{
    return Array_GetCount((array_s*)hImageList);
}

size_t _imglist_add(void* hImageList, void* pImg, SIZE_T dwBytes, int nIndex)
{
    size_t ret = 0;

    EXHANDLE hImg = _img_createfrommemory(pImg, dwBytes, NULL);
    if (hImg)
    {
        if (nIndex <= 0)
        {
            nIndex = Array_GetCount((array_s*)hImageList) + 1;
        }
        ret = Array_AddMember((array_s*)hImageList, hImg, nIndex);
        _img_destroy(hImg);
    }
    return ret;
}

BOOL _imglist_setimage(void* hImageList, int nIndex, EXHANDLE hImg)
{
    BOOL result = FALSE;

    if (hImg)
        result = Array_SetMember((array_s*)hImageList, nIndex, hImg);
    return result;
}

int _imglist_size(void* hImageList, int* pWidth, int* pHeight)
{
    int nSize = Array_GetExtra((array_s*)hImageList);

    if (nSize)
    {
        if (pWidth)
        {
            *pWidth = LOWORD(nSize);
        }
        if (pHeight)
        {
            *pHeight = HIWORD(nSize);
        }
    }
    return nSize;
}

size_t _imglist_addimage(void* hImageList, int hImg, int nIndex)
{
    size_t ret = 0;
    if (hImg)
    {
        if (nIndex <= 0)
        {
            nIndex = Array_GetCount((array_s*)hImageList) + 1;
        }
        ret = Array_AddMember((array_s*)hImageList, hImg, nIndex);
    }
    return ret;
}

int _imglist_array_addmember(array_s* hArr, int nIndex, void* pvItem, int nType)
{
    return _img_scale((EXHANDLE)pvItem, LOWORD(nType), HIWORD(nType));
}

int _imglist_array_delmember(array_s* hArr, int nIndex, void* pvItem, int nType)
{
    return _img_destroy((EXHANDLE)pvItem);
}
void* _imglist_create(int width, int height)
{
    array_s* hImageList;
    hImageList = Array_Create(0);
    Array_SetType(hImageList, MAKELONG(width, height));
    Array_SetExtra(hImageList, MAKELONG(width, height));
    Array_BindEvent(hImageList, eae_addmember, _imglist_array_addmember);
    Array_BindEvent(hImageList, eae_setmember, _imglist_array_addmember);
    Array_BindEvent(hImageList, eae_delmember, _imglist_array_delmember);
    return hImageList;
}

size_t _imglist_get(void* hImageList, size_t nIndex)
{
    return Array_GetMember((array_s*)hImageList, nIndex);
}