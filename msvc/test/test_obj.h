#pragma once
#include "help_ex.h"
#define Random(min,max) (rand()%(max-min))+ min + 1
#define COLOR_EX_CBTN_CRBKG_NORMAL 100
#define COLOR_EX_CBTN_CRBKG_HOVER 101
#define COLOR_EX_CBTN_CRBKG_DOWN 102
#define COLOR_EX_CBTN_CRBKG_FOCUS 103
#define EOUL_CBTN_CRBKG_NORMAL 0
#define EOUL_CBTN_CRBKG_HOVER 1
#define EOUL_CBTN_CRBKG_DOWN 2
#define EOUL_CBTN_CRBKG_FOCUS 3

#define SBM_SETVISIBLE 56212

typedef enum _WINDOWCOMPOSITIONATTRIB
{
	WCA_UNDEFINED = 0,
	WCA_NCRENDERING_ENABLED = 1,
	WCA_NCRENDERING_POLICY = 2,
	WCA_TRANSITIONS_FORCEDISABLED = 3,
	WCA_ALLOW_NCPAINT = 4,
	WCA_CAPTION_BUTTON_BOUNDS = 5,
	WCA_NONCLIENT_RTL_LAYOUT = 6,
	WCA_FORCE_ICONIC_REPRESENTATION = 7,
	WCA_EXTENDED_FRAME_BOUNDS = 8,
	WCA_HAS_ICONIC_BITMAP = 9,
	WCA_THEME_ATTRIBUTES = 10,
	WCA_NCRENDERING_EXILED = 11,
	WCA_NCADORNMENTINFO = 12,
	WCA_EXCLUDED_FROM_LIVEPREVIEW = 13,
	WCA_VIDEO_OVERLAY_ACTIVE = 14,
	WCA_FORCE_ACTIVEWINDOW_APPEARANCE = 15,
	WCA_DISALLOW_PEEK = 16,
	WCA_CLOAK = 17,
	WCA_CLOAKED = 18,
	WCA_ACCENT_POLICY = 19,
	WCA_FREEZE_REPRESENTATION = 20,
	WCA_EVER_UNCLOAKED = 21,
	WCA_VISUAL_OWNER = 22,
	WCA_LAST = 23
} WINDOWCOMPOSITIONATTRIB;

typedef struct _WINDOWCOMPOSITIONATTRIBDATA
{
	WINDOWCOMPOSITIONATTRIB Attrib;
	PVOID pvData;
	SIZE_T cbData;
} WINDOWCOMPOSITIONATTRIBDATA;

typedef enum _ACCENT_STATE
{
	ACCENT_DISABLED = 0,
	ACCENT_ENABLE_GRADIENT = 1,
	ACCENT_ENABLE_TRANSPARENTGRADIENT = 2,
	ACCENT_ENABLE_BLURBEHIND = 3,
	ACCENT_INVALID_STATE = 4
} ACCENT_STATE;

typedef struct _ACCENT_POLICY
{
	ACCENT_STATE AccentState;
	DWORD AccentFlags;
	DWORD GradientColor;
	DWORD AnimationId;
} ACCENT_POLICY;

typedef BOOL(WINAPI* pfnSetWindowCompositionAttribute)(HWND, WINDOWCOMPOSITIONATTRIBDATA*);

size_t CALLBACK button_proc(EXHANDLE hObj, int nID, int nCode, WPARAM wParam, LPARAM lParam);
void test_button(HWND hWnd);
void test_label(HWND hWnd);
void test_checkbutton(HWND hWnd);
void test_edit(HWND hWnd);
void test_listview(HWND hWnd);
void test_menubutton(HWND hWnd);
void test_custombkg(HWND hWnd);
void test_combobox(HWND hWnd);
void test_groupbox(HWND hWnd);
void test_messagebox(HWND hWnd);
void test_colorbutton(HWND hWnd);


void test_tab(HWND hWnd);
void TabBox_Change(int nIndex);
int TabBox_OnChangeAni(void* pEasingProgress, double nProgress, double nCurrent, void* pEasingContext, int nTimeSurplus, size_t p1, size_t p2, size_t p3, size_t p4);
size_t CALLBACK TabHeaderBtn_OnCheck(EXHANDLE hObj, int nID, int nCode, WPARAM wParam, LPARAM lParam);


void test_absolute(HWND hWnd);
void test_relative(HWND hWnd);
void test_linear(HWND hWnd);
void test_flow(HWND hWnd);
void test_table(HWND hWnd);



size_t CALLBACK OnAniWinProc(HWND hWnd, EXHANDLE handle, UINT uMsg, size_t wParam, size_t lParam, int* lpResult);
size_t CALLBACK OnWinAni(void* pEasing, double nProgress, double nCurrent, void* pEasingContext, int nTimeSurplus, size_t p1, size_t p2, size_t p3, size_t p4);
size_t CALLBACK AniButton_OnCLICK(EXHANDLE hObj, int nID, int nCode, WPARAM wParam, LPARAM lParam);
void AniShow(bool fShow);
void test_ani(HWND hWnd);

size_t CALLBACK OnCustomRedraw(HWND hWnd, EXHANDLE handle, UINT uMsg, size_t wParam, size_t lParam, int* lpResult);
void test_customredraw(HWND hWnd);

size_t CALLBACK _colorbutton_proc(HWND hWnd, EXHANDLE hObj, UINT uMsg, size_t wParam, size_t lParam);
void test_colorbutton(HWND hWnd);

HRESULT SetAero(HWND hWnd);
void test_aero(HWND hWnd);

void test_reportlistview(HWND hWnd);
size_t CALLBACK OnListEvent(EXHANDLE hObj, int nID, int nCode, WPARAM wParam, LPARAM lParam);

size_t CALLBACK OnIconWndProc(HWND hWnd, EXHANDLE handle, UINT uMsg, size_t wParam, size_t lParam, int* lpResult);
void test_iconlistview(HWND hWnd);