#include "test_obj.h"
#include "resource.h"

#include <dwmapi.h>
#pragma comment(lib, "dwmapi.lib")

EXHANDLE hExDui_button;

size_t CALLBACK button_proc(EXHANDLE hObj, int nID, int nCode, WPARAM wParam, LPARAM lParam)
{
	if (nID == 201)//通过组件ID判断按钮
	{
		Ex_ObjEnable(hObj, false);//禁用自身
		Ex_ObjSetPadding(hObj, 0, 20, 5, 5, 5, true);
	}
	else if (nID == 202)
	{
		Ex_ObjEnable(Ex_ObjGetFromID(hExDui_button, 201), true);//通过组件ID取按钮句柄，解除按钮1禁用
	}
	else if (nID == 203)
	{
		Ex_ObjSetText(hObj, L"自身文本被改动", false);//改动自身文本
	}
	else if (nID == 204)
	{
		auto text_length = Ex_ObjGetTextLength(Ex_ObjGetFromID(hExDui_button, 201));//取按钮1文本长度
		std::wstring str;
		str.resize(text_length * 2 + 2);
		Ex_ObjGetText(Ex_ObjGetFromID(hExDui_button, 201), str.c_str(), text_length * 2);
		Ex_ObjSetText(hObj, (L"按钮1文本:" + str).c_str(), false);
	}
	else if (nID == 205)
	{
		EX_PAINTSTRUCT2 ecd{ 0 };
		RtlMoveMemory(&ecd, (void*)lParam, sizeof(EX_PAINTSTRUCT2));
		int crBkg = 0;
		if ((ecd.dwState & STATE_DOWN) != 0)
		{
			crBkg = ExRGB2ARGB(255, 51);
		}
		else if ((ecd.dwState & STATE_HOVER) != 0)
		{
			crBkg = ExRGB2ARGB(16754943, 51);
		}
		else {
			crBkg = ExRGB2ARGB(16777215, 51);
		}
		auto hBrush = _brush_create(crBkg);
		if (hBrush)
		{
			_canvas_fillrect(ecd.hCanvas, hBrush, 0, 0, ecd.p_right, ecd.p_bottom);
			_brush_destroy(hBrush);
		}
	}
	return 0;
}


void test_button(HWND hWnd)
{
	auto hWnd_button = Ex_WndCreate(hWnd, L"Ex_DirectUI", L"测试按钮", 0, 0, 400, 300, 0, 0);
	hExDui_button = Ex_DUIBindWindowEx(hWnd_button, 0, EWS_NOINHERITBKG | EWS_BUTTON_CLOSE | EWS_BUTTON_MIN | EWS_MOVEABLE | EWS_CENTERWINDOW | EWS_TITLE | EWS_SIZEABLE | EWS_HASICON, 0, 0);
	Ex_DUISetLong(hExDui_button, EWL_CRBKG, ExARGB(120, 120, 120, 255));
	std::vector<EXHANDLE> buttons;
	buttons.push_back(Ex_ObjCreateEx(-1, L"button", L"禁用自身", -1, 10, 30, 120, 30, hExDui_button, 201, DT_VCENTER | DT_CENTER, 0, 0, NULL));
	buttons.push_back(Ex_ObjCreateEx(-1, L"button", L"解除按钮1禁用", -1, 10, 70, 120, 30, hExDui_button, 202, DT_VCENTER | DT_CENTER, 0, 0, NULL));
	buttons.push_back(Ex_ObjCreateEx(-1, L"button", L"改动自身文本", -1, 10, 110, 120, 30, hExDui_button, 203, DT_VCENTER | DT_CENTER, 0, 0, NULL));
	buttons.push_back(Ex_ObjCreateEx(-1, L"button", L"取按钮1文本", -1, 10, 150, 120, 30, hExDui_button, 204, DT_VCENTER | DT_CENTER, 0, 0, NULL));

	auto customdrawbutton = Ex_ObjCreateEx(EOS_EX_FOCUSABLE | EOS_EX_CUSTOMDRAW, L"button", L"自绘按钮", -1, 10, 190, 120, 30, hExDui_button, 205, DT_VCENTER | DT_CENTER, 0, 0, NULL);
	Ex_ObjHandleEvent(customdrawbutton, NM_CUSTOMDRAW, button_proc);

	for (auto button : buttons)
	{
		Ex_ObjHandleEvent(button, NM_CLICK, button_proc);
	}
	Ex_DUIShowWindow(hExDui_button, SW_SHOWNORMAL, 0, 0, 0);
}


void test_label(HWND hWnd)
{
	auto hWnd_label = Ex_WndCreate(hWnd, L"Ex_DirectUI", L"测试标签", 0, 0, 400, 300, 0, 0);
	auto hExDui_label = Ex_DUIBindWindowEx(hWnd_label, 0, EWS_NOINHERITBKG | EWS_BUTTON_CLOSE | EWS_BUTTON_MIN | EWS_MOVEABLE | EWS_CENTERWINDOW | EWS_TITLE | EWS_SIZEABLE | EWS_HASICON, 0, 0);
	Ex_DUISetLong(hExDui_label, EWL_CRBKG, ExARGB(220, 220, 220, 255));
	auto label = Ex_ObjCreateEx(-1, L"static", NULL, -1, 10, 30, 180, 150, hExDui_label, 0, DT_VCENTER, 0, 0, NULL);
	std::vector<char> imgdata;
	Ex_ReadFile(L".\\bkg.png", &imgdata);
	Ex_ObjSetBackgroundImage(label, imgdata.data(), imgdata.size(), 0, 0, BIR_DEFAULT, 0, BIF_PLAYIMAGE, 255, true);
	bkgimg_s bkg{ 0 };
	Ex_ObjGetBackgroundImage(label, &bkg);
	output(L"背景信息:", bkg.x_, bkg.y_, bkg.dwAlpha_, bkg.dwRepeat_, bkg.hImage_, bkg.curFrame_, bkg.maxFrame_);
	RECT rect;
	Ex_ObjGetRect(label, &rect);
	output(L"标签矩形:", rect.right, rect.bottom);

	auto label2 = Ex_ObjCreateEx(-1, L"static", NULL, -1, 200, 30, 150, 150, hExDui_label, 0, DT_VCENTER, 0, 0, NULL);
	std::vector<char> imgdata2;
	Ex_ReadFile(L".\\Loading.gif", &imgdata2);
	Ex_ObjSetBackgroundImage(label2, imgdata2.data(), imgdata2.size(), 0, 0, BIR_DEFAULT, 0, BIF_PLAYIMAGE, 255, true);

	Ex_DUIShowWindow(hExDui_label, SW_SHOWNORMAL, 0, 0, 0);
}

void test_checkbutton(HWND hWnd)
{
	auto hWnd_checkbutton = Ex_WndCreate(hWnd, L"Ex_DirectUI", L"测试单选框选择框", 0, 0, 400, 300, 0, 0);
	auto hExDui_checkbutton = Ex_DUIBindWindowEx(hWnd_checkbutton, 0, EWS_NOINHERITBKG | EWS_BUTTON_CLOSE | EWS_BUTTON_MIN | EWS_MOVEABLE | EWS_CENTERWINDOW | EWS_TITLE | EWS_SIZEABLE | EWS_HASICON, 0, 0);
	Ex_DUISetLong(hExDui_checkbutton, EWL_CRBKG, ExARGB(150, 150, 150, 255));

	auto checkbutton = Ex_ObjCreateEx(-1, L"checkbutton", L"选择框", -1, 10, 30, 60, 20, hExDui_checkbutton, 0, DT_VCENTER, 0, 0, NULL);
	auto radiobuttona = Ex_ObjCreateEx(-1, L"radiobutton", L"单选框1", -1, 10, 60, 60, 20, hExDui_checkbutton, 0, DT_VCENTER, 0, 0, NULL);
	auto radiobuttonb = Ex_ObjCreateEx(-1, L"radiobutton", L"单选框2", -1, 80, 60, 60, 20, hExDui_checkbutton, 0, DT_VCENTER, 0, 0, NULL);
	Ex_DUIShowWindow(hExDui_checkbutton, SW_SHOWNORMAL, 0, 0, 0);
}

void test_edit(HWND hWnd)
{
	auto hWnd_edit = Ex_WndCreate(hWnd, L"Ex_DirectUI", L"测试编辑框", 0, 0, 600, 300, 0, 0);
	auto hExDui_edit = Ex_DUIBindWindowEx(hWnd_edit, 0, EWS_BUTTON_CLOSE | EWS_BUTTON_MIN | EWS_MOVEABLE | EWS_CENTERWINDOW | EWS_TITLE | EWS_SIZEABLE | EWS_HASICON, 0, 0);
	
	Ex_DUISetLong(hExDui_edit, EWL_BLUR, 20);

	auto edit = Ex_ObjCreateEx(EOS_EX_FOCUSABLE | EOS_EX_COMPOSITED | EOS_EX_CUSTOMDRAW, L"edit", L"背景图片编辑框", EOS_VISIBLE | EES_HIDESELECTION, 10, 30, 150, 30, hExDui_edit, 0, DT_VCENTER, 0, 0, NULL);
	std::vector<char> imgdata;
	Ex_ReadFile(L".\\00000.jpg", &imgdata);
	Ex_ObjSetBackgroundImage(edit, imgdata.data(), imgdata.size(), 0, 0, BIR_DEFAULT, 0, BIF_DEFAULT, 255, true);
	auto edit2 = Ex_ObjCreateEx(EOS_EX_FOCUSABLE | EOS_EX_COMPOSITED, L"edit", L"测试密码输入编辑框", EOS_VISIBLE | EES_USEPASSWORD, 10, 70, 150, 30, hExDui_edit, 0, DT_SINGLELINE, 0, 0, NULL);
	auto edit3 = Ex_ObjCreateEx(EOS_EX_FOCUSABLE | EOS_EX_COMPOSITED, L"edit", L"测试数值输入编辑框", EOS_VISIBLE | EES_NUMERICINPUT, 10, 110, 150, 30, hExDui_edit, 0, DT_SINGLELINE, 0, 0, NULL);
	auto edit4 = Ex_ObjCreateEx(EOS_EX_FOCUSABLE | EOS_EX_COMPOSITED, L"edit", L"测试只读编辑框", EOS_VISIBLE | EES_READONLY, 10, 150, 150, 30, hExDui_edit, 0, DT_SINGLELINE, 0, 0, NULL);
	auto edit5 = Ex_ObjCreateEx(EOS_EX_FOCUSABLE | EOS_EX_COMPOSITED, L"edit", L"测试多行编辑框\r\n测试多行编辑框\r\n测试多行编辑框\r\n测试多行编辑框\r\n测试多行编辑框\r\n测试多行编辑框\r\n测试多行编辑框\r\n测试多行编辑框\r\n测试多行编辑框\r\n测试多行编辑框\r\n测试多行编辑框\r\n测试多行编辑框\r\n测试多行编辑框\r\n测试多行编辑框\r\n测试多行编辑框\r\n测试多行编辑框\r\n", EOS_VISIBLE | EOS_VSCROLL, 180, 30, 150, 200, hExDui_edit, 0, DT_VCENTER, 0, 0, NULL);
	auto edit6 = Ex_ObjCreateEx(EOS_EX_FOCUSABLE | EOS_EX_COMPOSITED, L"edit", NULL, EOS_VISIBLE | EOS_VSCROLL | EOS_HSCROLL | EES_RICHTEXT, 360, 30, 200, 200, hExDui_edit, 0, DT_LEFT | DT_TOP, 0, 0, NULL);

	std::vector<char> rtf;
	Ex_ReadFile(L".\\test.rtf", &rtf);
	Ex_ObjSendMessage(edit6, EM_LOAD_RTF, rtf.size(), (size_t)rtf.data());

	auto edit_transparent = Ex_ObjCreateEx(EOS_EX_FOCUSABLE | EOS_EX_COMPOSITED | EOS_EX_TABSTOP | EOS_EX_CUSTOMDRAW, L"edit", L"测试透明圆角编辑框", EOS_VISIBLE | EES_HIDESELECTION, 10, 190, 150, 40, hExDui_edit, 0, DT_VCENTER, 0, 0, NULL);
	Ex_ObjSetColor(edit_transparent, COLOR_EX_BACKGROUND, ExARGB(120, 130, 200, 100), false);
	Ex_ObjSetColor(edit_transparent, COLOR_EX_TEXT_NORMAL, ExRGB2ARGB(16872215, 255), false);
	Ex_ObjSetRadius(edit_transparent, 10, 10, 10, 0, false);

	Ex_DUIShowWindow(hExDui_edit, SW_SHOWNORMAL, 0, 0, 0);
}

void test_menubutton(HWND hWnd)
{
	auto hWnd_menubutton = Ex_WndCreate(hWnd, L"Ex_DirectUI", L"测试菜单按钮", 0, 0, 400, 300, 0, 0);
	auto hExDui_menubutton = Ex_DUIBindWindowEx(hWnd_menubutton, 0, EWS_NOINHERITBKG | EWS_BUTTON_CLOSE | EWS_BUTTON_MIN | EWS_MOVEABLE | EWS_CENTERWINDOW | EWS_TITLE | EWS_SIZEABLE | EWS_HASICON, 0, 0);
	Ex_DUISetLong(hExDui_menubutton, EWL_CRBKG, ExARGB(150, 150, 150, 255));

	EXHANDLE hObjMenuBar = Ex_ObjCreate(L"Page", 0, -1, 0, 30, 400, 22, hExDui_menubutton);
	if (hObjMenuBar != 0) {
		EXHANDLE hLayout = _layout_create(ELT_LINEAR, hObjMenuBar);
		HMENU hMenu = LoadMenu(GetModuleHandle(0), (LPWSTR)IDR_MENU1);
		if (hMenu) {
			for (int i = 0; i < GetMenuItemCount(hMenu); i++) {
				WCHAR wzText[256];
				GetMenuString(hMenu, i, wzText, 256, MF_BYPOSITION);
				EXHANDLE hObj = Ex_ObjCreateEx(-1, L"MenuButton", wzText, -1, 0, 0, 50, 22, hObjMenuBar, 0, -1, (size_t)GetSubMenu(hMenu, i), 0, 0);
				if (hObj) {
					Ex_ObjSetColor(hObj, COLOR_EX_BACKGROUND, 0, false);
					Ex_ObjSetColor(hObj, COLOR_EX_TEXT_HOVER, ExARGB(255, 255, 255, 50), false);
					Ex_ObjSetColor(hObj, COLOR_EX_TEXT_DOWN, ExARGB(255, 255, 255, 100), false);
					Ex_ObjSetColor(hObj, COLOR_EX_TEXT_NORMAL, ExARGB(255, 255, 255, 255), false);
					_layout_addchild(hLayout, hObj);
				}
			}
		}
		Ex_ObjLayoutSet(hObjMenuBar, hLayout, true);
	}
	Ex_DUIShowWindow(hExDui_menubutton, SW_SHOWNORMAL, 0, 0, 0);
}

void test_custombkg(HWND hWnd)
{
	auto hWnd_custombkg = Ex_WndCreate(hWnd, L"Ex_DirectUI", L"", 0, 0, 175, 200, 0, 0);
	auto hExDui_custombkg = Ex_DUIBindWindowEx(hWnd_custombkg, 0,  EWS_NOINHERITBKG | EWS_MOVEABLE | EWS_CENTERWINDOW  | EWS_NOSHADOW, 0, 0);
	std::vector<char> imgdata;
	Ex_ReadFile(L".\\custombkg.png", &imgdata);
	RECT grid = { 45,40,15,15 };
	Ex_ObjSetBackgroundImage(hExDui_custombkg, imgdata.data(), imgdata.size(), 0, 0, BIR_DEFAULT, &grid, BIF_DEFAULT, 220, true);
	Ex_ObjCreateEx(EOS_EX_TOPMOST, L"sysbutton", L"", EOS_VISIBLE | EWS_BUTTON_CLOSE, 140, 8, 30, 30, hExDui_custombkg, 300, 0, 0, 0, NULL);
	Ex_DUIShowWindow(hExDui_custombkg, SW_SHOWNORMAL, 0, 0, 0);
}


int CALLBACK list_proc(HWND hWnd, EXHANDLE hObj, UINT uMsg, size_t wParam, size_t lParam, int* lpResult)
{
	if (uMsg == WM_NOTIFY)
	{
		EX_NMHDR ni{ 0 };
		RtlMoveMemory(&ni, (void*)lParam, sizeof(EX_NMHDR));
		if (hObj == ni.hObjFrom)
		{
			if (ni.nCode == NM_CALCSIZE)
			{
				__set_int((void*)ni.lParam, 4, 25);
				*lpResult = 1;
				return 1;
			}
			else if (ni.nCode == NM_CUSTOMDRAW)
			{
				EX_CUSTOMDRAW cd{ 0 };
				RtlMoveMemory(&cd, (void*)ni.lParam, sizeof(EX_CUSTOMDRAW));
				if (cd.iItem > 0 && cd.iItem <= 100)
				{
					int crItemBkg = 0;
					if ((cd.dwState & STATE_SELECT) != 0)
					{
						crItemBkg = ExRGB2ARGB(16777215, 255);
					}
					else if ((cd.dwState & STATE_HOVER) != 0)
					{
						crItemBkg = ExRGB2ARGB(16777215, 150);
					}
					if (crItemBkg != 0)
					{
						void* hBrush = _brush_create(crItemBkg);
						_canvas_fillrect(cd.hCanvas, hBrush, cd.rcPaint.left, cd.rcPaint.top, cd.rcPaint.right, cd.rcPaint.bottom);
						_brush_destroy(hBrush);
					}
					_canvas_drawtext(cd.hCanvas, Ex_ObjGetFont(hObj), ExRGB2ARGB(0, 180), L"你好123", -1, DT_SINGLELINE | DT_VCENTER, cd.rcPaint.left + 10, cd.rcPaint.top, cd.rcPaint.right, cd.rcPaint.bottom);
				}
				*lpResult = 1;
				return 1;
			}
			else if (ni.nCode == LVN_ITEMCHANGED)
			{
				//wParam 新选中项,lParam 旧选中项
				output(L"改变选中ID:", ni.idFrom, L"新选中项:", ni.wParam, L"旧选中项:", ni.lParam);
			}
		}
	}

	return 0;
}

size_t OnScrollBarMsg(HWND hWND, EXHANDLE hObj, UINT uMsg, WPARAM wParam, LPARAM lParam,int* lpResult)
{
	if (uMsg == WM_MOUSEHOVER)
	{
		Ex_ObjPostMessage(hObj, SBM_SETVISIBLE, 0, 1);
	}
	else if (uMsg == WM_MOUSELEAVE)
	{
		Ex_ObjPostMessage(hObj, SBM_SETVISIBLE, 0, 0);
	}
	else if (uMsg == SBM_SETVISIBLE)
	{
		Ex_ObjSetLong(hObj, EOL_ALPHA, lParam != 0 ? 255 : 0);
		Ex_ObjInvalidateRect(hObj, 0);
	}
	return 0;
}

void test_listview(HWND hWnd)
{
	auto hWnd_listview = Ex_WndCreate(hWnd, L"Ex_DirectUI", L"测试列表框", 0, 0, 400, 300, 0, 0);
	auto hExDui_listview = Ex_DUIBindWindowEx(hWnd_listview, 0, EWS_NOINHERITBKG | EWS_BUTTON_CLOSE | EWS_BUTTON_MIN | EWS_MOVEABLE | EWS_CENTERWINDOW | EWS_TITLE | EWS_SIZEABLE | EWS_HASICON, 0, 0);
	Ex_DUISetLong(hExDui_listview, EWL_CRBKG, ExARGB(150, 150, 150, 255));

	auto listview = Ex_ObjCreateEx(EOS_EX_COMPOSITED, L"listview", NULL, EOS_VISIBLE | ELS_VERTICALLIST | EOS_VSCROLL, 130, 30, 150, 200, hExDui_listview, 0, -1, 0, 0, list_proc);
	Ex_ObjSetColor(listview, COLOR_EX_BACKGROUND, ExARGB(255, 255, 255, 150), true);
	Ex_ObjSendMessage(listview, LVM_SETITEMCOUNT, 100, 100);

	auto hObjScroll = Ex_ObjScrollGetControl(listview, SB_VERT);
	Ex_ObjPostMessage(hObjScroll, SBM_SETVISIBLE, 0, 0);
	Ex_ObjSetLong(hObjScroll, EOL_OBJPROC, (size_t)OnScrollBarMsg);

	Ex_DUIShowWindow(hExDui_listview, SW_SHOWNORMAL, 0, 0, 0);
}

void test_groupbox(HWND hWnd)
{
	auto hWnd_groupbox = Ex_WndCreate(hWnd, L"Ex_DirectUI", L"测试分组框", 0, 0, 400, 300, 0, 0);
	auto hExDui_groupbox = Ex_DUIBindWindowEx(hWnd_groupbox, 0, EWS_NOINHERITBKG | EWS_BUTTON_CLOSE | EWS_BUTTON_MIN | EWS_MOVEABLE | EWS_CENTERWINDOW | EWS_TITLE | EWS_SIZEABLE | EWS_HASICON, 0, 0);
	Ex_DUISetLong(hExDui_groupbox, EWL_CRBKG, ExARGB(150, 150, 150, 255));

	auto groupbox = Ex_ObjCreate(L"groupbox", L"分组框", -1, 30, 30, 230, 230, hExDui_groupbox);
	Ex_ObjSetColor(groupbox, COLOR_EX_TEXT_NORMAL, ExARGB(255, 55, 55, 255), false);
	Ex_ObjSetColor(groupbox, COLOR_EX_BORDER, ExARGB(55, 0, 250, 255), false);
	Ex_ObjSetLong(groupbox, GROUPBOX_TEXT_OFFSET, 50);
	Ex_ObjSetLong(groupbox, GROUPBOX_RADIUS, 30);
	Ex_ObjSetLong(groupbox, GROUPBOX_STROKEWIDTH, 3);
	Ex_DUIShowWindow(hExDui_groupbox, SW_SHOWNORMAL, 0, 0, 0);
}

int CALLBACK TabHeaderBtn_Proc(HWND hWnd, EXHANDLE hObj, UINT uMsg, size_t wParam, size_t lParam, int* lpResult)
{
	EX_PAINTSTRUCT2 ps;
	if (uMsg == WM_PAINT)
	{
		if (Ex_ObjBeginPaint(hObj, &ps))
		{
			
			_canvas_drawtext(ps.hCanvas, Ex_ObjGetFont(hObj), ExRGB2ARGB(0, 255), (LPCWSTR)Ex_ObjGetLong(hObj, EOL_LPWZTITLE), -1, ps.dwTextFormat, ps.p_left, ps.p_top, ps.p_right, ps.p_bottom);
			if ((ps.dwState & STATE_CHECKED) == STATE_CHECKED)
			{
				auto brush = _brush_create(ExRGB2ARGB(255, 255));
				_canvas_drawline(ps.hCanvas, brush, ps.p_left, ps.p_bottom, ps.p_right, ps.p_bottom, 3, D2D1_DASH_STYLE_SOLID);
				_brush_destroy(brush);
			}
			Ex_ObjEndPaint(hObj, &ps);
		}
		*lpResult = 1;
		return 1;
	}
	return 0;
}

int m_nCurPage = 1;
std::vector<EXHANDLE> m_hObjPages;
EXHANDLE m_hLayout = 0;
EXHANDLE m_hObj_Tab = 0;

int TabBox_OnChangeAni(void* pEasingProgress, double nProgress, double nCurrent, void* pEasingContext, int nTimeSurplus, size_t p1, size_t p2, size_t p3, size_t p4)
{

	for (int i = 0; i < m_hObjPages.size(); i++)
	{
		_layout_setchildprop(m_hLayout, m_hObjPages[i], ELCP_ABSOLUTE_LEFT, (nCurrent - i - 1) * 100);
	}
	Ex_ObjLayoutUpdate(m_hObj_Tab);
	return 0;
}

void TabBox_Change(int nIndex)
{
	if (m_nCurPage != nIndex)
	{
		//output(L"TabBox_Change->选中", m_nCurPage, nIndex);
		_easing_create(ET_InOutQuart, 0, ES_SINGLE | ES_CALLFUNCTION | ES_THREAD, (size_t)TabBox_OnChangeAni, 250, 25, EES_PLAY, m_nCurPage, nIndex, 0, 0, 0, 0);
		m_nCurPage = nIndex;
	}
}



size_t CALLBACK TabHeaderBtn_OnCheck(EXHANDLE hObj, int nID, int nCode, WPARAM wParam, LPARAM lParam)
{
	
	TabBox_Change(nID - 300);
	return 0;
}



void test_tab(HWND hWnd)
{
	auto hWnd_tab = Ex_WndCreate(hWnd, L"Ex_DirectUI", L"测试选项卡", 0, 0, 400, 300, 0, 0);
	auto hExDui_tab = Ex_DUIBindWindowEx(hWnd_tab, 0, EWS_NOINHERITBKG | EWS_BUTTON_CLOSE | EWS_BUTTON_MIN | EWS_MOVEABLE | EWS_CENTERWINDOW | EWS_TITLE | EWS_SIZEABLE | EWS_HASICON, 0, 0);
	
	auto layout = _layout_create(ELT_ABSOLUTE, hExDui_tab);

	m_hObj_Tab = Ex_ObjCreateEx(-1, L"page", NULL, -1, 25, 40, 350, 250, hExDui_tab, 0, -1, 0, 0, 0);
	_layout_absolute_lock(layout, m_hObj_Tab, ELCP_ABSOLUTE_TYPE_PX, ELCP_ABSOLUTE_TYPE_PX, ELCP_ABSOLUTE_TYPE_PX, ELCP_ABSOLUTE_TYPE_PX, ELCP_ABSOLUTE_TYPE_UNKNOWN, ELCP_ABSOLUTE_TYPE_UNKNOWN);//锁住tabbox的左顶右底
	auto m_hLayout = _layout_create(ELT_ABSOLUTE, m_hObj_Tab);
	_layout_setprop(m_hLayout, ELP_PADDING_TOP, 30);
	for (int i = 0; i < 5; i++)
	{
		auto str = L"Tab" + std::to_wstring(i);
		auto hObj_tab=Ex_ObjCreateEx(EOS_EX_FOCUSABLE | EOS_EX_CUSTOMDRAW, L"RadioButton", str.c_str(), -1, i * 60, 0, 60, 30, m_hObj_Tab, 300 + i + 1, DT_CENTER | DT_VCENTER | DT_SINGLELINE, 0, 0, TabHeaderBtn_Proc);
		Ex_ObjHandleEvent(hObj_tab, NM_CHECK, TabHeaderBtn_OnCheck);
		auto hObj = Ex_ObjCreateEx(-1, L"Static", str.c_str(), -1, 0, 0, 0, 0, m_hObj_Tab, 0, DT_CENTER | DT_VCENTER, 0, 0, 0);
		Ex_ObjSetColor(hObj, COLOR_EX_BACKGROUND, ExRGB2ARGB(i % 2 == 0 ? 16777215 : 0, 255), true);
		_layout_absolute_setedge(m_hLayout, hObj, ELCP_ABSOLUTE_LEFT, ELCP_ABSOLUTE_TYPE_PS, i * 100);
		_layout_absolute_setedge(m_hLayout, hObj, ELCP_ABSOLUTE_TOP, ELCP_ABSOLUTE_TYPE_PX, 0);
		_layout_absolute_setedge(m_hLayout, hObj, ELCP_ABSOLUTE_WIDTH, ELCP_ABSOLUTE_TYPE_PS, 100);
		_layout_absolute_setedge(m_hLayout, hObj, ELCP_ABSOLUTE_HEIGHT, ELCP_ABSOLUTE_TYPE_PS, 100);
		m_hObjPages.push_back(hObj);
	}
	Ex_ObjLayoutSet(m_hObj_Tab, m_hLayout, true);
	Ex_ObjSendMessage(Ex_ObjGetFromID(hExDui_tab, 301), BM_SETCHECK, 1, 0);
	m_nCurPage = 1;
	Ex_ObjLayoutSet(hExDui_tab, layout, true);
	Ex_DUISetLong(hExDui_tab, EWL_CRBKG, ExRGB2ARGB(16777215, 255));
	Ex_DUIShowWindow(hExDui_tab, SW_SHOWNORMAL, 0, 0, 0);
}

void test_absolute(HWND hWnd)
{
	auto hWnd_absolute = Ex_WndCreate(hWnd, L"Ex_DirectUI", L"测试绝对布局", 0, 0, 400, 300, 0, 0);
	auto hExDui_absolute = Ex_DUIBindWindowEx(hWnd_absolute, 0, EWS_NOINHERITBKG | EWS_BUTTON_CLOSE| EWS_BUTTON_MAX | EWS_BUTTON_MIN | EWS_MOVEABLE | EWS_CENTERWINDOW | EWS_TITLE | EWS_SIZEABLE | EWS_HASICON, 0, 0);
	Ex_DUISetLong(hExDui_absolute, EWL_CRBKG, ExRGB2ARGB(16777215, 255));
	auto hLayout = _layout_create(ELT_ABSOLUTE, hExDui_absolute);

	auto hObj = Ex_ObjCreate(L"Static", L"固定在右下角50,50的位置，不变大小", -1, 0, 0, 200, 100, hExDui_absolute);
	Ex_ObjSetColor(hObj, COLOR_EX_BACKGROUND, ExRGB2ARGB(255, 100), true);
	_layout_absolute_setedge(hLayout, hObj, ELCP_ABSOLUTE_RIGHT, ELCP_ABSOLUTE_TYPE_PX, 50);//设置距离右边为50像素
	_layout_absolute_setedge(hLayout, hObj, ELCP_ABSOLUTE_BOTTOM, ELCP_ABSOLUTE_TYPE_PX, 50);// 设置距离底边为50像素

	auto hObj2 = Ex_ObjCreate(L"Static", L"固定在左下角50,50的位置，宽度为40%,高度为50", -1, 0, 0, 200, 100, hExDui_absolute);
	Ex_ObjSetColor(hObj2, COLOR_EX_BACKGROUND, ExRGB2ARGB(0, 100), true);
	_layout_absolute_setedge(hLayout, hObj2, ELCP_ABSOLUTE_LEFT, ELCP_ABSOLUTE_TYPE_PX, 50);//设置距离左边为50像素
	_layout_absolute_setedge(hLayout, hObj2, ELCP_ABSOLUTE_BOTTOM, ELCP_ABSOLUTE_TYPE_PX, 50);// 设置距离底边为50像素
	_layout_absolute_setedge(hLayout, hObj2, ELCP_ABSOLUTE_WIDTH, ELCP_ABSOLUTE_TYPE_PS, 40);// 注意单位是PS（百分比）
	_layout_absolute_setedge(hLayout, hObj2, ELCP_ABSOLUTE_HEIGHT, ELCP_ABSOLUTE_TYPE_PX, 50);

	auto hObj3 = Ex_ObjCreate(L"Static", L"距离四边均为20%", -1, 0, 0, 0, 0, hExDui_absolute);
	Ex_ObjSetColor(hObj3, COLOR_EX_BACKGROUND, ExRGB2ARGB(16711680, 100), true);
	_layout_absolute_setedge(hLayout, hObj3, ELCP_ABSOLUTE_LEFT, ELCP_ABSOLUTE_TYPE_PS, 20);//注意单位是PS（百分比）
	_layout_absolute_setedge(hLayout, hObj3, ELCP_ABSOLUTE_TOP, ELCP_ABSOLUTE_TYPE_PS, 20);// 注意单位是PS（百分比）
	_layout_absolute_setedge(hLayout, hObj3, ELCP_ABSOLUTE_RIGHT, ELCP_ABSOLUTE_TYPE_PS, 20);// 注意单位是PS（百分比）
	_layout_absolute_setedge(hLayout, hObj3, ELCP_ABSOLUTE_BOTTOM, ELCP_ABSOLUTE_TYPE_PS, 20);// 注意单位是PS（百分比）
	
	auto hObj4 = Ex_ObjCreate(L"Static", L"居中于窗口,宽度为窗口的30%,高度为100像素", -1, 0, 0, 0, 0, hExDui_absolute);
	Ex_ObjSetColor(hObj4, COLOR_EX_BACKGROUND, ExRGB2ARGB(65280, 100), true);
	_layout_absolute_setedge(hLayout, hObj4, ELCP_ABSOLUTE_LEFT, ELCP_ABSOLUTE_TYPE_PS, 50);//注意单位是PS（百分比）
	_layout_absolute_setedge(hLayout, hObj4, ELCP_ABSOLUTE_TOP, ELCP_ABSOLUTE_TYPE_PS, 50);// 注意单位是PS（百分比）
	_layout_absolute_setedge(hLayout, hObj4, ELCP_ABSOLUTE_WIDTH, ELCP_ABSOLUTE_TYPE_PS, 30);// 注意单位是PS（百分比）
	_layout_absolute_setedge(hLayout, hObj4, ELCP_ABSOLUTE_HEIGHT, ELCP_ABSOLUTE_TYPE_PX, 100);// 
	_layout_absolute_setedge(hLayout, hObj4, ELCP_ABSOLUTE_OFFSET_H, ELCP_ABSOLUTE_TYPE_OBJPS, -50);//  水平偏移控件-50%的控件宽度 注意单位是OBJ_PS（控件尺寸的百分比）
	_layout_absolute_setedge(hLayout, hObj4, ELCP_ABSOLUTE_OFFSET_V, ELCP_ABSOLUTE_TYPE_OBJPS, -50);//  水平偏移控件-50%的控件高度 注意单位是OBJ_PS（控件尺寸的百分比）

	Ex_ObjLayoutSet(hExDui_absolute, hLayout, true);
	Ex_DUIShowWindow(hExDui_absolute, SW_SHOWNORMAL, 0, 0, 0);
}

void test_relative(HWND hWnd)
{
	auto hWnd_relative = Ex_WndCreate(hWnd, L"Ex_DirectUI", L"测试相对布局", 0, 0, 600, 400, 0, 0);
	auto hExDui_relative = Ex_DUIBindWindowEx(hWnd_relative, 0, EWS_NOINHERITBKG | EWS_BUTTON_CLOSE | EWS_BUTTON_MAX | EWS_BUTTON_MIN | EWS_MOVEABLE | EWS_CENTERWINDOW | EWS_TITLE | EWS_SIZEABLE | EWS_HASICON, 0, 0);
	Ex_DUISetLong(hExDui_relative, EWL_CRBKG, ExRGB2ARGB(16777215, 255));
	auto hLayout = _layout_create(ELT_RELATIVE, hExDui_relative);
	_layout_setprop(hLayout, ELP_PADDING_LEFT, 10);
	_layout_setprop(hLayout, ELP_PADDING_TOP, 25);
	_layout_setprop(hLayout, ELP_PADDING_RIGHT, 10);
	_layout_setprop(hLayout, ELP_PADDING_BOTTOM, 10);

	auto hObj = Ex_ObjCreateEx(-1,L"Static", L"控件A：父容器的左下角", -1, 0, 0, 200, 150, hExDui_relative,0,DT_VCENTER,0,0,0);
	Ex_ObjSetColor(hObj, COLOR_EX_BACKGROUND, ExARGB(255,0,0, 100), true);
	_layout_setchildprop(hLayout, hObj, ELCP_RELATIVE_LEFT_ALIGN_OF, -1);//左侧与父容器对齐
	_layout_setchildprop(hLayout, hObj, ELCP_RELATIVE_BOTTOM_ALIGN_OF, -1);// 底边与父容器对齐

	auto hObj2 = Ex_ObjCreateEx(-1, L"Static", L"控件B：父容器居中顶部", -1, 0, 0, 200, 150, hExDui_relative, 0, DT_VCENTER, 0, 0, 0);
	Ex_ObjSetColor(hObj2, COLOR_EX_BACKGROUND, ExRGB2ARGB(16711680, 100), true);
	_layout_setchildprop(hLayout, hObj2, ELCP_RELATIVE_TOP_ALIGN_OF, -1);//顶部与父容器对齐
	_layout_setchildprop(hLayout, hObj2, ELCP_RELATIVE_CENTER_PARENT_H, 1);// 水平居中于父容器

	auto hObj3 = Ex_ObjCreateEx(-1, L"Static", L"控件C：右侧与A对齐,宽度150,在A和B之间", -1, 0, 0, 150, 150, hExDui_relative, 0, DT_VCENTER, 0, 0, 0);
	Ex_ObjSetColor(hObj3, COLOR_EX_BACKGROUND, ExRGB2ARGB(65280, 100), true);
	_layout_setchildprop(hLayout, hObj3, ELCP_RELATIVE_TOP_OF, hObj);//在A控件顶部
	_layout_setchildprop(hLayout, hObj3, ELCP_RELATIVE_BOTTOM_OF, hObj2);// 在B控件底部
	_layout_setchildprop(hLayout, hObj3, ELCP_RELATIVE_RIGHT_ALIGN_OF, hObj);// 在B右侧对齐于A控件

	auto hObj4 = Ex_ObjCreateEx(-1, L"Static", L"控件D：高度100,在A和父控件右边界之间,在父容器底部", -1, 0, 0, 150, 100, hExDui_relative, 0, DT_VCENTER, 0, 0, 0);
	Ex_ObjSetColor(hObj4, COLOR_EX_BACKGROUND, ExRGB2ARGB(16754943, 100), true);
	_layout_setchildprop(hLayout, hObj4, ELCP_RELATIVE_RIGHT_OF, hObj);//在A控件右边
	_layout_setchildprop(hLayout, hObj4, ELCP_RELATIVE_BOTTOM_ALIGN_OF, -1);// 底部对齐于父容器
	_layout_setchildprop(hLayout, hObj4, ELCP_RELATIVE_RIGHT_ALIGN_OF, -1);// 右侧对齐于父容器
	
	auto hObj5 = Ex_ObjCreateEx(-1, L"Static", L"控件E：与D同宽,在D和B之间", -1, 0, 0, 150, 100, hExDui_relative, 0, DT_VCENTER, 0, 0, 0);
	Ex_ObjSetColor(hObj5, COLOR_EX_BACKGROUND, ExRGB2ARGB(8445952, 100), true);
	_layout_setchildprop(hLayout, hObj5, ELCP_RELATIVE_TOP_OF, hObj4);//在D顶部
	_layout_setchildprop(hLayout, hObj5, ELCP_RELATIVE_BOTTOM_OF, hObj2);// 在B底部
	_layout_setchildprop(hLayout, hObj5, ELCP_RELATIVE_LEFT_ALIGN_OF, hObj4);// 左侧对齐于D
	_layout_setchildprop(hLayout, hObj5, ELCP_RELATIVE_RIGHT_ALIGN_OF, hObj4);// 右侧对齐于D

	auto hObj6 = Ex_ObjCreateEx(-1, L"Static", L"控件F：150宽度,垂直方向对齐于DE,右对齐于DE", -1, 0, 0, 150, 100, hExDui_relative, 0, DT_VCENTER, 0, 0, 0);
	Ex_ObjSetColor(hObj6, COLOR_EX_BACKGROUND, ExRGB2ARGB(16777215, 100), true);
	_layout_setchildprop(hLayout, hObj6, ELCP_RELATIVE_TOP_ALIGN_OF, hObj5);//顶部对齐于E
	_layout_setchildprop(hLayout, hObj6, ELCP_RELATIVE_BOTTOM_ALIGN_OF, hObj4);// 底部对齐于D
	_layout_setchildprop(hLayout, hObj6, ELCP_RELATIVE_RIGHT_ALIGN_OF, hObj4);// 右对齐于D


	Ex_ObjLayoutSet(hExDui_relative, hLayout, true);
	RECT rect{ 0 };
	Ex_ObjGetRect(hObj4, &rect);
	Ex_DUIShowWindow(hExDui_relative, SW_SHOWNORMAL, 0, 0, 0);
	

}

void test_linear(HWND hWnd)
{
	auto hWnd_linear = Ex_WndCreate(hWnd, L"Ex_DirectUI", L"测试线性布局", 0, 0, 600, 400, 0, 0);
	auto hExDui_linear = Ex_DUIBindWindowEx(hWnd_linear, 0, EWS_NOINHERITBKG | EWS_BUTTON_CLOSE | EWS_BUTTON_MAX | EWS_BUTTON_MIN | EWS_MOVEABLE | EWS_CENTERWINDOW | EWS_TITLE | EWS_SIZEABLE | EWS_HASICON, 0, 0);
	Ex_DUISetLong(hExDui_linear, EWL_CRBKG, ExRGB2ARGB(16777215, 255));
	auto hLayout = _layout_create(ELT_LINEAR, hExDui_linear);
	_layout_setprop(hLayout, ELP_LINEAR_DIRECTION, ELP_DIRECTION_H); //设置布局方向为水平
	_layout_setprop(hLayout, ELP_LINEAR_DALIGN, ELP_LINEAR_DALIGN_CENTER);// 设置布局方向对齐方式为居中

	auto hObj = Ex_ObjCreateEx(-1, L"Static", L"容器1", -1, 0, 0, 200,300, hExDui_linear, 0, DT_VCENTER, 0, 0, 0);
	Ex_ObjSetColor(hObj, COLOR_EX_BACKGROUND, ExRGB2ARGB(255, 100), true);
	_layout_setchildprop(hLayout, hObj, ELCP_LINEAR_ALIGN, ELCP_LINEAR_ALIGN_CENTER);//设置居中于父
	_layout_setchildprop(hLayout, hObj, ELCP_MARGIN_RIGHT, 10);// 设置右边10像素间隔

	auto hObj2 = Ex_ObjCreateEx(-1, L"Static", L"容器2", -1, 0, 0, 400, 200, hExDui_linear, 0, DT_VCENTER, 0, 0, 0);
	Ex_ObjSetColor(hObj2, COLOR_EX_BACKGROUND, ExRGB2ARGB(16711680, 100), true);
	_layout_setchildprop(hLayout, hObj2, ELCP_LINEAR_ALIGN, ELCP_LINEAR_ALIGN_CENTER);//设置居中于父
	_layout_setchildprop(hLayout, hObj2, ELCP_MARGIN_RIGHT, 10);// 设置右边10像素间隔

	auto hObj3 = Ex_ObjCreateEx(-1, L"Static", L"容器3", -1, 0, 0, 100, 100, hExDui_linear, 0, DT_VCENTER, 0, 0, 0);
	Ex_ObjSetColor(hObj3, COLOR_EX_BACKGROUND, ExRGB2ARGB(65280, 100), true);
	_layout_setchildprop(hLayout, hObj3, ELCP_LINEAR_ALIGN, ELCP_LINEAR_ALIGN_CENTER);//设置居中于父


	Ex_ObjLayoutSet(hExDui_linear, hLayout, true);
	Ex_DUIShowWindow(hExDui_linear, SW_SHOWNORMAL, 0, 0, 0);
}

void test_flow(HWND hWnd)
{
	auto hWnd_flow = Ex_WndCreate(hWnd, L"Ex_DirectUI", L"测试流式布局", 0, 0, 600, 400, 0, 0);
	auto hExDui_flow = Ex_DUIBindWindowEx(hWnd_flow, 0, EWS_NOINHERITBKG | EWS_BUTTON_CLOSE | EWS_BUTTON_MAX | EWS_BUTTON_MIN | EWS_MOVEABLE | EWS_CENTERWINDOW | EWS_TITLE | EWS_SIZEABLE | EWS_HASICON, 0, 0);
	Ex_DUISetLong(hExDui_flow, EWL_CRBKG, ExRGB2ARGB(16777215, 255));
	auto hLayout = _layout_create(ELT_FLOW, hExDui_flow);
	_layout_setprop(hLayout, ELP_FLOW_DIRECTION, ELP_DIRECTION_V);//设置布局方向为垂直

	_layout_setprop(hLayout, ELP_PADDING_LEFT, 30);
	_layout_setprop(hLayout, ELP_PADDING_TOP, 30); //设置距离顶边30
	_layout_setprop(hLayout, ELP_PADDING_RIGHT, 30);
	_layout_setprop(hLayout, ELP_PADDING_BOTTOM, 30);

	EXHANDLE hObj = 0;
	for (int i = 0; i < 20; i++)
	{
		hObj = Ex_ObjCreate(L"Static", L"test", -1, 0, 0, Random(50, 150), Random(50, 150), hExDui_flow);
		Ex_ObjSetColor(hObj, COLOR_EX_BACKGROUND, ExRGB2ARGB(255, 100), true);
		_layout_setchildprop(hLayout, hObj, ELCP_MARGIN_RIGHT, 10);
		_layout_setchildprop(hLayout, hObj, ELCP_MARGIN_BOTTOM, 10);
		if (i % 10 == 0)
		{
			_layout_setchildprop(hLayout, hObj, ELCP_FLOW_NEW_LINE, 1);
		}
	}
	Ex_ObjLayoutSet(hExDui_flow, hLayout, true);

	Ex_DUIShowWindow(hExDui_flow, SW_SHOWNORMAL, 0, 0, 0);
}

void test_table(HWND hWnd)
{
	auto hWnd_table = Ex_WndCreate(hWnd, L"Ex_DirectUI", L"测试表格布局", 0, 0, 600, 400, 0, 0);
	auto hExDui_table  = Ex_DUIBindWindowEx(hWnd_table, 0, EWS_NOINHERITBKG | EWS_BUTTON_CLOSE | EWS_BUTTON_MAX | EWS_BUTTON_MIN | EWS_MOVEABLE | EWS_CENTERWINDOW | EWS_TITLE | EWS_SIZEABLE | EWS_HASICON, 0, 0);
	Ex_DUISetLong(hExDui_table, EWL_CRBKG, ExRGB2ARGB(16777215, 255));
	auto hLayout = _layout_create(ELT_TABLE, hExDui_table);

	_layout_setprop(hLayout, ELP_PADDING_LEFT, 10);
	_layout_setprop(hLayout, ELP_PADDING_TOP, 30);
	_layout_setprop(hLayout, ELP_PADDING_RIGHT, 10);
	_layout_setprop(hLayout, ELP_PADDING_BOTTOM, 10);
	int row[4] = { 50,-30,75,-20 };//4行,正数为像素,负数为百分比
	int cell[3] = { 100,75,-50 };//3列,正数为像素,负数为百分比
	EXHANDLE hObj = 0;
	_layout_table_setinfo(hLayout, row, 4, cell, 3);
	for (int i = 1; i <=4; i++)
	{
		for (int j = 1; j <= 3; j++)
		{
			auto str = std::to_wstring(i) + L"," + std::to_wstring(j);
			hObj = Ex_ObjCreateEx(-1, L"static", (LPCWSTR)str.c_str(), -1, 0, 0, 200, 150, hExDui_table, 0, DT_VCENTER, 0, 0, 0);
			Ex_ObjSetColor(hObj, COLOR_EX_BACKGROUND, ExRGB2ARGB(255, 100), false);
			_layout_setchildprop(hLayout, hObj, ELCP_TABLE_ROW, i); // 设置所属行
			_layout_setchildprop(hLayout, hObj, ELCP_TABLE_CELL, j); //设置所属列
		}
	}
	auto hObj2 = Ex_ObjCreateEx(-1, L"static",L"(2,1)[占2行]", -1, 0, 0, 200, 150, hExDui_table, 0, DT_VCENTER, 0, 0, 0);
	Ex_ObjSetColor(hObj2, COLOR_EX_BACKGROUND, ExRGB2ARGB(65535, 150), true);
	_layout_setchildprop(hLayout, hObj2, ELCP_TABLE_CELL, 2);
	_layout_setchildprop(hLayout, hObj2, ELCP_TABLE_ROW, 1);
	_layout_setchildprop(hLayout, hObj2, ELCP_TABLE_ROW_SPAN, 2);//设置跨行数

	auto hObj3 = Ex_ObjCreateEx(-1, L"static", L"(1,3)[占3列2行]", -1, 0, 0, 200, 150, hExDui_table, 0, DT_VCENTER, 0, 0, 0);
	Ex_ObjSetColor(hObj3, COLOR_EX_BACKGROUND, ExRGB2ARGB(16711935, 120), true);
	_layout_setchildprop(hLayout, hObj3, ELCP_TABLE_CELL, 1);
	_layout_setchildprop(hLayout, hObj3, ELCP_TABLE_ROW, 3);
	_layout_setchildprop(hLayout, hObj3, ELCP_TABLE_ROW_SPAN, 2);
	_layout_setchildprop(hLayout, hObj3, ELCP_TABLE_CELL_SPAN, 3);//设置跨列数

	Ex_ObjLayoutSet(hExDui_table, hLayout, true);

	Ex_DUIShowWindow(hExDui_table, SW_SHOWNORMAL, 0, 0, 0);
}

void test_combobox(HWND hWnd)
{
	auto hWnd_combobox = Ex_WndCreate(hWnd, L"Ex_DirectUI", L"测试组合框", 0, 0, 600, 400, 0, 0);
	auto hExDui_combobox = Ex_DUIBindWindowEx(hWnd_combobox, 0, EWS_NOINHERITBKG | EWS_BUTTON_CLOSE  | EWS_BUTTON_MIN | EWS_MOVEABLE | EWS_CENTERWINDOW | EWS_TITLE | EWS_SIZEABLE | EWS_HASICON, 0, 0);
	Ex_DUISetLong(hExDui_combobox, EWL_CRBKG, ExARGB(150, 150, 150, 255));
	auto combobox = Ex_ObjCreateEx(-1, L"combobox", L"测试组合框", EOS_VISIBLE | ECS_ALLOWEDIT, 10, 30, 100, 30, hExDui_combobox, 0, DT_VCENTER, 0, 0, NULL);
	Ex_ObjSendMessage(combobox, CB_ADDSTRING, 0, (size_t)L"TEST");
	Ex_ObjSendMessage(combobox, CB_ADDSTRING, 0, (size_t)L"1234");
	Ex_ObjSendMessage(combobox, CB_ADDSTRING, 0, (size_t)L"5679");
	Ex_ObjSendMessage(combobox, CB_ADDSTRING, 0, (size_t)L"qwer");
	Ex_ObjSendMessage(combobox, CB_ADDSTRING, 0, (size_t)L"rtyuu");
	Ex_ObjSendMessage(combobox, CB_ADDSTRING, 0, (size_t)L"uiop[");
	Ex_ObjSendMessage(combobox, CB_ADDSTRING, 0, (size_t)L"asdf");
	Ex_ObjSendMessage(combobox, CB_ADDSTRING, 0, (size_t)L"fgkk!l");
	Ex_ObjSendMessage(combobox, CB_ADDSTRING, 0, (size_t)L"zcx！v");
	Ex_ObjSendMessage(combobox, CB_ADDSTRING, 0, (size_t)L"cv他bnm");
	Ex_ObjSendMessage(combobox, CB_ADDSTRING, 0, (size_t)L".jkjk");
	Ex_ObjSendMessage(combobox, CB_ADDSTRING, 0, (size_t)L"ervb");
	Ex_ObjSendMessage(combobox, CB_ADDSTRING, 0, (size_t)L"45你5yy");
	Ex_ObjSendMessage(combobox, CB_ADDSTRING, 0, (size_t)L"h6h5h56h6");
	Ex_ObjSendMessage(combobox, CB_ADDSTRING, 0, (size_t)L"bbbbbbb");
	Ex_ObjSendMessage(combobox, CB_ADDSTRING, 0, (size_t)L"5我555555");
	Ex_ObjSendMessage(combobox, CB_ADDSTRING, 0, (size_t)L"222222");
	Ex_ObjSendMessage(combobox, CB_ADDSTRING, 0, (size_t)L"cvadf");

	Ex_DUIShowWindow(hExDui_combobox, SW_SHOWNORMAL, 0, 0, 0);
}

HWND hWnd_ani = 0;
EXHANDLE hExDui_ani = 0;
easing_s* easing3 = nullptr;

size_t CALLBACK OnAniWinProc(HWND hWnd, EXHANDLE handle, UINT uMsg, size_t wParam, size_t lParam, int* lpResult)
{
	if (uMsg == WM_CLOSE)
	{
		AniShow(false);
	}
	return 0;
}

size_t CALLBACK OnWinAni(void* pEasing, double nProgress, double nCurrent, void* pEasingContext, int nTimeSurplus, size_t p1, size_t p2, size_t p3, size_t p4)
{
	
	int index = nCurrent * 255;
	Ex_DUISetLong(hExDui_ani, EWL_CRBKG, ExRGB2ARGB(16746496, index));
	Ex_DUISetLong(hExDui_ani, EWL_ALPHA, index);
	SetWindowPos((HWND)Ex_DUIGetLong(hExDui_ani, EWL_HWND), 0, p1 + p2 * nCurrent, p3 + p4 * nCurrent, 0, 0, SWP_NOSIZE | SWP_NOACTIVATE | SWP_NOZORDER);
	return 0;
}

void AniShow(bool fShow)
{
	Ex_DUISetLong(hExDui_ani, EWL_CRBKG, ExRGB2ARGB(16746496, fShow ? 0 : 255));
	Ex_DUISetLong(hExDui_ani, EWL_ALPHA, fShow ? 0 : 255);
	Ex_DUIShowWindow(hExDui_ani, SW_SHOW, 0, 0, 0);
	
	RECT rc{ 0 };
	GetWindowRect((HWND)Ex_DUIGetLong(hExDui_ani, EWL_HWND),&rc);
	_easing_create(ET_InOutCubic, 0, ES_SINGLE | ES_CALLFUNCTION | (fShow ? 0 : ES_REVERSE), (size_t)OnWinAni, 500, 20, EES_PLAY, 0, 1, rc.left, 0, rc.top - 100, 100);
	Ex_DUIShowWindow(hExDui_ani, fShow ? SW_SHOW: SW_HIDE, 0, 0,0);
}

size_t CALLBACK OnBtnEasing(void* pEasing, double nProgress, double nCurrent, void* pEasingContext, int nTimeSurplus, size_t p1, size_t p2, size_t p3, size_t p4)
{

	Ex_ObjSetPos(p1, 0, 0, 0, nCurrent, 50, SWP_NOMOVE | SWP_NOZORDER);
	return 0;
}

size_t CALLBACK OnBtnEasing4(void* pEasing, double nProgress, double nCurrent, void* pEasingContext, int nTimeSurplus, size_t p1, size_t p2, size_t p3, size_t p4)
{
	RECT rc{ 0 };
	GetWindowRect(hWnd_ani, &rc);
	MoveWindow(hWnd_ani, rc.left, rc.top, nCurrent, rc.bottom - rc.top, true);
	return 0;
}

size_t CALLBACK AniButton_OnEvent(EXHANDLE hObj, int nID, int nCode, WPARAM wParam, LPARAM lParam)
{
	if (nID == 10001)
	{
		if (nCode == NM_CLICK)
		{
			_easing_create(ET_InOutCubic, 0, ES_SINGLE | ES_THREAD | ES_CALLFUNCTION, (size_t)OnBtnEasing, 200, 20, EES_PLAY, 150, 300, hObj, 0, 0, 0);
		}
	}
	else if (nID == 10002)
	{
		if (nCode == NM_CLICK)
		{
			_easing_create(ET_InOutCubic, 0,MAKELONG( ES_MANYTIMES | ES_BACKANDFORTH | ES_THREAD | ES_DISPATCHNOTIFY,10*2), hObj, 150, 20, EES_PLAY, 150, 300, 0, 0, 0, 0);
		}
		else if (nCode == NM_EASING)
		{
			EX_EASINGINFO pEasingInfo{ 0 };
			sizeof(EX_EASINGINFO);
			RtlMoveMemory(&pEasingInfo, (void*)lParam, sizeof(EX_EASINGINFO));
			Ex_ObjSetPos(hObj, 0, 0, 0, pEasingInfo.nCurrent, 50, SWP_NOMOVE | SWP_NOZORDER);
		}
	}
	else if (nID == 10003)
	{
		if (nCode == NM_CLICK)
		{
			if (_easing_getstate(easing3) == EES_PAUSE)
			{
				_easing_setstate(easing3, EES_PLAY);
			}
			else {
				_easing_setstate(easing3, EES_PAUSE);
			}
		}
		else if (nCode == NM_DESTROY)
		{
			_easing_setstate(easing3, EES_STOP);
		}
		else if (nCode == NM_EASING)
		{
			EX_EASINGINFO pEasingInfo{ 0 };
			sizeof(EX_EASINGINFO);
			RtlMoveMemory(&pEasingInfo, (void*)lParam, sizeof(EX_EASINGINFO));
			Ex_ObjSetPos(hObj, 0, 0, 0, pEasingInfo.nCurrent, 50, SWP_NOMOVE | SWP_NOZORDER);
		}
	}
	else if (nID == 10004)
	{
		if (nCode == NM_CLICK)
		{
			_easing_create(ET_InOutCirc, 0, MAKELONG(ES_MANYTIMES | ES_BACKANDFORTH | ES_CALLFUNCTION, 4), (size_t)OnBtnEasing4, 600, 25, 0, 600, 250, 0, 0, 0, 0);
		}
	}
	return 0;
}

void test_ani(HWND hWnd)
{
	hWnd_ani = Ex_WndCreate(hWnd, L"Ex_DirectUI", L"测试缓动窗口", 0, 0, 600, 400, 0, 0);
	hExDui_ani = Ex_DUIBindWindowEx(hWnd_ani, 0, EWS_NOINHERITBKG | EWS_BUTTON_CLOSE  | EWS_BUTTON_MIN | EWS_MOVEABLE | EWS_CENTERWINDOW | EWS_TITLE | EWS_SIZEABLE | EWS_HASICON, 0, OnAniWinProc);
	auto hObj1 = Ex_ObjCreateEx(-1, L"button", L"点击就动1次", -1, 10, 40, 120, 50, hExDui_ani, 10001, DT_VCENTER | DT_CENTER, 0, 0, NULL);
	auto hObj2 = Ex_ObjCreateEx(-1, L"button", L"来回", -1, 10, 100, 120, 50, hExDui_ani, 10002, DT_VCENTER | DT_CENTER, 0, 0, NULL);
	auto hObj3 = Ex_ObjCreateEx(-1, L"button", L"点击动/停", -1, 10, 160, 120, 50, hExDui_ani, 10003, DT_VCENTER | DT_CENTER, 0, 0, NULL);
	auto hObj4 = Ex_ObjCreateEx(-1, L"button", L"点击窗口动", -1, 10, 220, 120, 50, hExDui_ani, 10004, DT_VCENTER | DT_CENTER, 0, 0, NULL);
	Ex_ObjHandleEvent(hObj1, NM_CLICK, AniButton_OnEvent);
	Ex_ObjHandleEvent(hObj2, NM_CLICK, AniButton_OnEvent);
	Ex_ObjHandleEvent(hObj2, NM_EASING, AniButton_OnEvent);
	Ex_ObjHandleEvent(hObj3, NM_CLICK, AniButton_OnEvent);
	Ex_ObjHandleEvent(hObj3, NM_EASING, AniButton_OnEvent);
	Ex_ObjHandleEvent(hObj3, NM_DESTROY, AniButton_OnEvent);
	Ex_ObjHandleEvent(hObj4, NM_CLICK, AniButton_OnEvent);
	 easing3 = _easing_create(ET_InOutCirc, 0, ES_CYCLE | ES_BACKANDFORTH | ES_THREAD | ES_DISPATCHNOTIFY, hObj3, 200, 20, EES_PAUSE, 150, 300, 0, 0, 0, 0);
	AniShow(true);	
}

HRESULT SetAero(HWND hWnd) {
	BOOL enabled;
	DWORD dwMajorVer;
	DWORD dwMinorVer;
	DWORD dwBuildNumber;
	if (GetNtVersionNumbers(dwMajorVer, dwMinorVer, dwBuildNumber)) {
		if (dwMajorVer == 6 && (dwMinorVer == 0 || dwMinorVer == 1)) {
			DwmIsCompositionEnabled(&enabled);
			if (enabled) {
				DWM_BLURBEHIND blur = { 0 };
				blur.dwFlags = 3;
				blur.fEnable = 1;
				return DwmEnableBlurBehindWindow(hWnd, &blur);
			}
		}
		else if (dwMajorVer == 10) {
			HMODULE hUser = GetModuleHandle(L"user32.dll");
			if (hUser)
			{
				pfnSetWindowCompositionAttribute setWindowCompositionAttribute = (pfnSetWindowCompositionAttribute)GetProcAddress(hUser, "SetWindowCompositionAttribute");
				if (setWindowCompositionAttribute)
				{
					ACCENT_POLICY accent = { ACCENT_ENABLE_BLURBEHIND, 0, 0, 0 };
					WINDOWCOMPOSITIONATTRIBDATA data;
					data.Attrib = WCA_ACCENT_POLICY;
					data.pvData = &accent;
					data.cbData = sizeof(accent);
					return setWindowCompositionAttribute(hWnd, &data);
				}
			}
		}
	}
	return 0;
}

void test_aero(HWND hWnd)
{
	auto hWnd_ani = Ex_WndCreate(hWnd, L"Ex_DirectUI", L"测试Aero", 0, 0, 600, 400, 0, 0);
	hExDui_ani = Ex_DUIBindWindowEx(hWnd_ani, 0, EWS_NOINHERITBKG | EWS_BUTTON_CLOSE | EWS_BUTTON_MIN | EWS_MOVEABLE | EWS_CENTERWINDOW | EWS_TITLE | EWS_SIZEABLE | EWS_HASICON, 0, NULL);
	Ex_DUISetLong(hExDui_ani, EWL_CRBKG, ExARGB(255, 255, 255, 1));


	SetAero(hWnd_ani);
	Ex_DUIShowWindow(hExDui_ani, SW_SHOWNORMAL, 0, 0, 0);
}

size_t CALLBACK OnCustomRedraw(HWND hWnd, EXHANDLE handle, UINT uMsg, size_t wParam, size_t lParam, int* lpResult)
{
	if (uMsg == WM_ERASEBKGND)
	{
		RECT rc{ 0 };
		GetWindowRect(hWnd, &rc);
		OffsetRect(&rc, -rc.left, -rc.top);
		_canvas_setantialias(wParam, true);
		_canvas_clear(wParam, 0);
		auto hBrush = _brush_create(ExRGB2ARGB(16711680, 150));
		_canvas_fillellipse(wParam, hBrush, rc.right / 2, rc.bottom / 2, rc.right / 2 - 2, rc.bottom /2 - 2);
		_brush_destroy(hBrush);
		*lpResult = 1;
		return 1;
	}
	return 0;
}

void test_customredraw(HWND hWnd)
{
	auto hWnd_customredraw = Ex_WndCreate(hWnd, L"Ex_DirectUI", L"", 0, 0,400, 300, 0, 0);
	auto hExDui_customredraw = Ex_DUIBindWindowEx(hWnd_customredraw, 0,EWS_MOVEABLE | EWS_CENTERWINDOW | EWS_NOSHADOW , 0, OnCustomRedraw);
	Ex_DUISetLong(hExDui_customredraw, EWL_CRBKG, ExARGB(150, 150, 150, 255));
	Ex_ObjCreateEx(EOS_EX_TOPMOST, L"sysbutton", L"", EOS_VISIBLE | EWS_BUTTON_CLOSE, (400-32)/2, (300-32)/2, 32, 32, hExDui_customredraw, 0, 0, 0, 0, NULL);

	Ex_DUIShowWindow(hExDui_customredraw, SW_SHOWNORMAL, 0, 0, 0);
}

void test_messagebox(HWND hWnd)
{
	auto hWnd_messagebox = Ex_WndCreate(hWnd, L"Ex_DirectUI", L"", 0, 0, 400, 300, 0, 0);
	auto hExDui_messagebox = Ex_DUIBindWindowEx(hWnd_messagebox, 0, EWS_NOINHERITBKG | EWS_MOVEABLE | EWS_CENTERWINDOW | EWS_NOSHADOW | EWS_BUTTON_CLOSE, 0, 0);
	Ex_DUISetLong(hExDui_messagebox, EWL_CRBKG, ExARGB(150, 150, 150, 255));
	Ex_MessageBox(hExDui_messagebox, L"test", L"testa", MB_YESNO | MB_ICONQUESTION, EMBF_CENTEWINDOW);

	Ex_DUIShowWindow(hExDui_messagebox, SW_SHOWNORMAL, 0, 0, 0);
}

size_t _colorbutton_paint(EXHANDLE hObj)
{
	EX_PAINTSTRUCT2 ps;
	if (Ex_ObjBeginPaint(hObj,&ps))
	{
		int crText = 0;
		int crBkg = 0;
		if ((ps.dwState & STATE_DOWN) == STATE_DOWN)
		{
			crText = Ex_ObjGetColor(hObj, COLOR_EX_TEXT_DOWN);
			crBkg = Ex_ObjGetLong(hObj, EOUL_CBTN_CRBKG_DOWN);
		}
		else if ((ps.dwState & STATE_HOVER) == STATE_HOVER)
		{
			crText = Ex_ObjGetColor(hObj, COLOR_EX_TEXT_HOVER);
			crBkg = Ex_ObjGetLong(hObj, EOUL_CBTN_CRBKG_HOVER);
		}
		else if ((ps.dwState & STATE_FOCUS) == STATE_FOCUS)
		{
			crText = Ex_ObjGetColor(hObj, COLOR_EX_TEXT_FOCUS);
			crBkg = Ex_ObjGetLong(hObj, EOUL_CBTN_CRBKG_FOCUS);
		}
		if (crBkg == 0)
		{
			crBkg = Ex_ObjGetLong(hObj, EOUL_CBTN_CRBKG_NORMAL);
		}
		if (crText == 0)
		{
			crText = Ex_ObjGetColor(hObj, COLOR_EX_TEXT_NORMAL);
		}
		_canvas_clear(ps.hCanvas, crBkg);
		_canvas_drawtext(ps.hCanvas, Ex_ObjGetFont(hObj), crText, (LPCWSTR)Ex_ObjGetLong(hObj, EOL_LPWZTITLE), -1, ps.dwTextFormat, 0, 0, ps.width, ps.height);
		Ex_ObjEndPaint(hObj, &ps);
	}
	return 0;
}

size_t CALLBACK _colorbutton_proc(HWND hWnd, EXHANDLE hObj, UINT uMsg, size_t wParam, size_t lParam)
{
	if (uMsg == WM_MOUSEHOVER)
	{
		Ex_ObjSetUIState(hObj, STATE_HOVER, false, 0, true);
	}
	else if (uMsg == WM_MOUSELEAVE)
	{
		Ex_ObjSetUIState(hObj, STATE_HOVER, true, 0, true);
	}
	else if (uMsg == WM_LBUTTONDOWN)
	{
		Ex_ObjSetUIState(hObj, STATE_DOWN, false, 0, true);
	}
	else if (uMsg == WM_LBUTTONUP)
	{
		Ex_ObjSetUIState(hObj, STATE_DOWN, true, 0, true);
	}
	else if (uMsg == WM_SETFOCUS)
	{
		Ex_ObjSetUIState(hObj, STATE_FOCUS, false, 0, true);
	}
	else if (uMsg == WM_KILLFOCUS)
	{
		Ex_ObjSetUIState(hObj, STATE_FOCUS, true, 0, true);
	}
	else if (uMsg == WM_SYSCOLORCHANGE)
	{
		if (wParam == COLOR_EX_CBTN_CRBKG_NORMAL)
		{
			Ex_ObjSetLong(hObj, EOUL_CBTN_CRBKG_NORMAL, lParam);
		}
		else if (wParam == COLOR_EX_CBTN_CRBKG_HOVER)
		{
			Ex_ObjSetLong(hObj, EOUL_CBTN_CRBKG_HOVER, lParam);
		}
		else if (wParam == COLOR_EX_CBTN_CRBKG_DOWN)
		{
			Ex_ObjSetLong(hObj, EOUL_CBTN_CRBKG_DOWN, lParam);
		}
		else if (wParam == COLOR_EX_CBTN_CRBKG_FOCUS)
		{
			Ex_ObjSetLong(hObj, EOUL_CBTN_CRBKG_FOCUS, lParam);
		}
	}
	else if (uMsg == WM_PAINT)
	{
		_colorbutton_paint(hObj);
	}
	return Ex_ObjDefProc(hWnd, hObj, uMsg, wParam, lParam);
}



void test_colorbutton(HWND hWnd)
{
	
	auto hWnd_colorbutton = Ex_WndCreate(hWnd, L"Ex_DirectUI", L"", 0, 0, 400, 300, 0, 0);
	auto hExDui_colorbutton = Ex_DUIBindWindowEx(hWnd_colorbutton, 0, EWS_NOINHERITBKG | EWS_MOVEABLE | EWS_CENTERWINDOW | EWS_NOSHADOW | EWS_BUTTON_CLOSE, 0, 0);
	Ex_DUISetLong(hExDui_colorbutton, EWL_CRBKG, ExARGB(150, 150, 150, 255));
	Ex_ObjRegister(L"colorbutton", EOS_VISIBLE, EOS_EX_TABSTOP | EOS_EX_FOCUSABLE, DT_SINGLELINE | DT_CENTER | DT_VCENTER, 4 * sizeof(void*), 0, 0, _colorbutton_proc);
	auto hObj = Ex_ObjCreateEx(-1, L"colorbutton", L"colorbutton", -1, 100, 100, 125, 50, hExDui_colorbutton, 0, -1, 0, 0, NULL);
	if (hObj != 0)
	{
		Ex_ObjSetColor(hObj, COLOR_EX_CBTN_CRBKG_NORMAL, ExRGB2ARGB(16777215, 255), false);
		Ex_ObjSetColor(hObj, COLOR_EX_CBTN_CRBKG_HOVER, ExRGB2ARGB(65535, 255), false);
		Ex_ObjSetColor(hObj, COLOR_EX_CBTN_CRBKG_DOWN, ExRGB2ARGB(65280, 255), false);
		Ex_ObjSetColor(hObj, COLOR_EX_CBTN_CRBKG_FOCUS, ExRGB2ARGB(16777680, 255), false);

		Ex_ObjSetColor(hObj, COLOR_EX_TEXT_HOVER, ExRGB2ARGB(255, 255), false);
		Ex_ObjSetColor(hObj, COLOR_EX_TEXT_DOWN, ExRGB2ARGB(16754943, 255), false);
		Ex_ObjSetColor(hObj, COLOR_EX_TEXT_FOCUS, ExRGB2ARGB(65280, 255), true);

		Ex_ObjSetRadius(hObj, 25, 25, 25, 25, true);
	}

	Ex_DUIShowWindow(hExDui_colorbutton, SW_SHOWNORMAL, 0, 0, 0);
}

size_t CALLBACK OnListEvent(EXHANDLE hObj, int nID, int nCode, WPARAM wParam, LPARAM lParam)
{
	if (nCode == LVN_ITEMCHANGED)
	{
		auto str = L"你选择了第" + std::to_wstring(wParam) + L"项";
		output(str);
	}
	else if (nCode == RLVN_COLUMNCLICK)
	{
		auto str = L"你点击了第" + std::to_wstring(wParam) + L"项";
		output(str);
	}
	else if (nCode == NM_CLICK)
	{
		wParam = Ex_ObjSendMessage(hObj, LVM_GETSELECTIONMARK, 0, 0);//获取现行选中项
		lParam = Ex_ObjSendMessage(hObj, RLVM_GETHITCOL, 0, LOWORD(lParam));//获取命中的列索引
		auto str = L"你点击了第" + std::to_wstring(wParam) + L"项,第" + std::to_wstring(lParam) + L"列";
		output(str);
	}
	else if (nCode == RLVN_DELETE_ITEM)
	{
		EX_REPORTLIST_ROWINFO row{ 0 };
		RtlMoveMemory(&row, (void*)lParam, sizeof(EX_REPORTLIST_ROWINFO));
		if (row.hImage != 0)//如果有图片，则销毁
		{
			_img_destroy(row.hImage);
		}
	}
	return 0;
}

void test_reportlistview(HWND hWnd)
{

	auto hWnd_reportlistview = Ex_WndCreate(hWnd, L"Ex_DirectUI", L"", 0, 0, 400, 300, 0, 0);
	auto hExDui_reportlistview = Ex_DUIBindWindowEx(hWnd_reportlistview, 0, EWS_NOINHERITBKG | EWS_MOVEABLE | EWS_CENTERWINDOW | EWS_NOSHADOW | EWS_BUTTON_CLOSE, 0, 0);
	Ex_DUISetLong(hExDui_reportlistview, EWL_CRBKG, ExARGB(150, 150, 150, 255));
	auto hObj = Ex_ObjCreateEx(-1, L"ReportListView", L"ReportListView", -1, 25, 50, 350, 225, hExDui_reportlistview, 0, -1, 0, 0, NULL);
	Ex_ObjSetColor(hObj, COLOR_EX_BACKGROUND, ExRGB2ARGB(16777215, 100), false);
	Ex_ObjSetColor(hObj, COLOR_EX_BORDER, ExRGB2ARGB(12632256, 100), false);
	Ex_ObjSetColor(hObj, COLOR_EX_TEXT_HOT, ExRGB2ARGB(16777215, 250), false);
	Ex_ObjSetColor(hObj, COLOR_EX_TEXT_HOVER, ExRGB2ARGB(12632256, 50), false);

	EX_REPORTLIST_COLUMNINFO col;
	col.wzText = L"第一列";
	col.nWidth = 75;
	col.crText = ExRGB2ARGB(255, 255);
	col.dwStyle = 0;
	col.dwTextFormat = DT_LEFT;
	Ex_ObjSendMessage(hObj, LVM_INSERTCOLUMN, 0, (size_t)&col);

	col.wzText = L"第二列";
	col.nWidth = 110;
	col.crText = ExRGB2ARGB(16711680, 255);
	col.dwStyle = ERLV_CS_LOCKWIDTH;
	col.dwTextFormat = DT_LEFT;
	Ex_ObjSendMessage(hObj, LVM_INSERTCOLUMN, 0, (size_t)&col);

	col.wzText = L"居中可点击";
	col.nWidth = 100;
	col.crText = ExRGB2ARGB(65535, 255);
	col.dwStyle = ERLV_CS_CLICKABLE;
	col.dwTextFormat = DT_CENTER | DT_VCENTER;
	Ex_ObjSendMessage(hObj, LVM_INSERTCOLUMN, 0, (size_t)&col);

	col.wzText = L"可排序";
	col.nWidth = 60;
	col.crText = ExRGB2ARGB(16777215, 255);
	col.dwStyle = ERLV_CS_CLICKABLE | ERLV_CS_SORTABLE;
	col.dwTextFormat = DT_RIGHT | DT_VCENTER;
	Ex_ObjSendMessage(hObj, LVM_INSERTCOLUMN, 0, (size_t)&col);


	EX_REPORTLIST_ROWINFO row;
	EX_REPORTLIST_ITEMINFO item;
	for (int i = 1; i <= 100; i++)
	{
		//先插入表项
		row.lParam = i;
		item.iRow = Ex_ObjSendMessage(hObj, LVM_INSERTITEM, 0, (size_t)&row);
		//先插入表项
		item.iCol = 1;
		item.wzText = L"第一列";
		Ex_ObjSendMessage(hObj, LVM_SETITEM, 0, (size_t)&item);//wParam为是否立即更新

		item.iCol = 2;
		item.wzText = L"第二列";
		Ex_ObjSendMessage(hObj, LVM_SETITEM, 0, (size_t)&item);//wParam为是否立即更新

		item.iCol = 3;
		item.wzText = L"第三列";
		Ex_ObjSendMessage(hObj, LVM_SETITEM, 0, (size_t)&item);//wParam为是否立即更新

		item.iCol = 4;
		auto str = std::to_wstring(Random(0, 1000));
		item.wzText = str.c_str();
		Ex_ObjSendMessage(hObj, LVM_SETITEM, 0, (size_t)&item);//wParam为是否立即更新
	}
	Ex_ObjSendMessage(hObj, LVM_UPDATE, 0, 0);//整体更新,以加快绘制速度
	Ex_ObjHandleEvent(hObj, LVN_ITEMCHANGED, OnListEvent);
	Ex_ObjHandleEvent(hObj, RLVN_COLUMNCLICK, OnListEvent);
	Ex_ObjHandleEvent(hObj, NM_CLICK, OnListEvent);



	Ex_DUIShowWindow(hExDui_reportlistview, SW_SHOWNORMAL, 0, 0, 0);
}

EXHANDLE m_hObjListView_icon=0;
void* m_hImageList_icon=0;

size_t CALLBACK OnIconBtnClick(EXHANDLE hObj, int nID, int nCode, WPARAM wParam, LPARAM lParam)
{

	return 0;
}

size_t CALLBACK OnIconWndProc(HWND hWnd, EXHANDLE handle, UINT uMsg, size_t wParam, size_t lParam, int* lpResult)
{
	if (uMsg == WM_SIZE)
	{
		Ex_ObjMove(m_hObjListView_icon, 25, 50, LOWORD(lParam) - 50, HIWORD(lParam) - 75, true);
	}
	else if (uMsg == WM_DESTROY)
	{
		_imglist_destroy(m_hImageList_icon);
	}
	return 0;
}

void test_iconlistview(HWND hWnd)
{

	auto hWnd_iconlistview = Ex_WndCreate(hWnd, L"Ex_DirectUI", L"", 0, 0, 600, 400, 0, 0);
	auto hExDui_iconlistview = Ex_DUIBindWindowEx(hWnd_iconlistview, 0, EWS_NOINHERITBKG | EWS_MOVEABLE | EWS_CENTERWINDOW | EWS_NOSHADOW | EWS_BUTTON_CLOSE, 0, OnIconWndProc);
	
	m_hObjListView_icon = Ex_ObjCreateEx(-1, L"iconlistview", L"iconlistview",EOS_VISIBLE | EOS_HSCROLL | EOS_VSCROLL | ILVS_BUTTON, 25, 50, 350, 225, hExDui_iconlistview, 0, -1, 0, 0, NULL);
	Ex_ObjSendMessage(m_hObjListView_icon, ILVM_SETITEMSIZE, 0, MAKELONG(70, 75));//设置表项尺寸为70,75
	//创建添加图片组信息

	m_hImageList_icon = _imglist_create(36, 36);
	std::vector<char> imgdata;
	for (int i = 1; i <= 3; i++)
	{
		auto str = L"./icon/" + std::to_wstring(i) + L".png";
		Ex_ReadFile(str, &imgdata);
		_imglist_add(m_hImageList_icon, imgdata.data(), imgdata.size(), 0);
	}
	//设置列表的图片组
	Ex_ObjSendMessage(m_hObjListView_icon, LVM_SETIMAGELIST, 0, (size_t)m_hImageList_icon);
	EX_ICONLISTVIEW_ITEMINFO ilvi{ 0 };
	
	for (int i = 1; i <= 1000; i++)
	{
		
		ilvi.nIndex = i;
		auto str = L"第" + std::to_wstring(i) + L"项";
		ilvi.pwzText = str.c_str();
		ilvi.nImageIndex = i % 3;
		if (ilvi.nImageIndex == 0) ilvi.nImageIndex = 3;
		Ex_ObjSendMessage(m_hObjListView_icon, LVM_INSERTITEMA, 0,(size_t)&ilvi);
	}
	Ex_ObjSendMessage(m_hObjListView_icon, LVM_UPDATE, 0, 0);
	Ex_ObjHandleEvent(m_hObjListView_icon, LVN_ITEMCHANGED, OnIconBtnClick);

	Ex_DUISetLong(hExDui_iconlistview, EWL_CRBKG, ExARGB(150, 150, 150, 255));
	Ex_DUIShowWindow(hExDui_iconlistview, SW_SHOWNORMAL, 0, 0, 0);
}